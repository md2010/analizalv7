﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XO
{
    public partial class Form2 : Form
    {
             
        string player1_name;
        string player2_name;
        Player player1;       
        Player player2;
        int next;
        int rounds; //= 1;
        bool player1_previous; //= false;
        bool player2_previous;//= true;       

        public Form2(string name1, string name2)
        {
            InitializeComponent();
            player1_name = name1;
            player2_name = name2;

            player1 = new Player(player1_name, 0,'x');
            player2 = new Player(player2_name, 0,'o');

            next = 1;
            rounds = 1;
            
            label3_name1.Text = player1.get_Name();
            label4_name2.Text = player2.get_Name();
            textBox2_score1.Text = player1.get_Wins().ToString();
            textBox3_score2.Text = player2.get_Wins().ToString();
            set_WhoNext();
            textBox1_round.Text = rounds.ToString();
        }   

        private void Form2_Load(object sender, EventArgs e)
        {
            player1_previous = false;
            player2_previous = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(next == 1)
            {
                button1.Text = player1.get_Sign().ToString();
            } if(next == 2)
            {
                button1.Text = player2.get_Sign().ToString();
            }
            button1.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button_2_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button_2.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button_2.Text = player2.get_Sign().ToString();
            }
            button_2.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button_3_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button_3.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button_3.Text = player2.get_Sign().ToString();
            }
            button_3.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button4.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button4.Text = player2.get_Sign().ToString();
            }
            button4.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button5.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button5.Text = player2.get_Sign().ToString();
            }
            button5.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button6.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button6.Text = player2.get_Sign().ToString();
            }
            button6.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button7.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button7.Text = player2.get_Sign().ToString();
            }
            button7.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button8.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button8.Text = player2.get_Sign().ToString();
            }
            button8.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (next == 1)
            {
                button9.Text = player1.get_Sign().ToString();
            }
            if (next == 2)
            {
                button9.Text = player2.get_Sign().ToString();
            }
            button9.Enabled = false;
            set_WhoNext();
            checkIfWon();
            
        }

        public void set_WhoNext()
        {           
            if (player1_previous == true)
            {
                next = 2;
                player2_previous = true;
                player1_previous = false;
            }
            else if (player1_previous == false)
            {
                next = 1;
                player1_previous = true;
                player2_previous = false;
            }

            display_NextPlayer(next);
        }
      
        public void display_NextPlayer(int next)
        {
            if (next == 1)
                textBox1_next.Text = player1.get_Name() + " is next!";
            else if (next == 2)
                textBox1_next.Text = player2.get_Name() + " is next!";
        }
        
        public void checkIfWon()
        {
            horizontal();
            vertical();
            diagonal();
            whoWon(" ");
        }

        public void horizontal()
        {
            //first row
            if (button1.Text == button_2.Text && button_3.Text == button_2.Text)
            {
                whoWon(button1.Text);
            }
            //second row
            if (button4.Text == button5.Text && button6.Text == button5.Text)
            {
                whoWon(button4.Text);
            }
            //third row
            if (button7.Text == button8.Text && button9.Text == button8.Text)
            {
                whoWon(button7.Text);
            }
        }

        public void vertical()
        {
            //first col
            if (button1.Text == button4.Text && button7.Text == button4.Text)
            {
                whoWon(button1.Text);
            }
            //second col
            if (button_2.Text == button5.Text && button8.Text == button5.Text)
            {
                whoWon(button_2.Text);
            }
            //third col
            if (button_3.Text == button6.Text && button9.Text == button6.Text)
            {
                whoWon(button_3.Text);
            }
        }

        public void diagonal()
        {           
            if (button1.Text == button5.Text && button9.Text == button5.Text)
            {
                whoWon(button1.Text);
            }

            if (button5.Text == button7.Text && button_3.Text == button7.Text)
            {
                whoWon(button7.Text);
            }                      
        }

        public void whoWon(string temp)
        {
            bool someone_won = false;
            if(temp == "x")
            {
                MessageBox.Show(player1_name + " won! To continue playing click the Next round button.");
                player1.set_Wins(1);
                textBox2_score1.Text = player1.get_Wins().ToString();
                rounds++;
                someone_won = true;
            }
            if (temp == "o")
            {
                MessageBox.Show(player2_name + " won! To continue playing click the Next round button.");
                player2.set_Wins(1);
                textBox3_score2.Text = player2.get_Wins().ToString();
                rounds++;
                someone_won = true;
            }if(temp == " " && someone_won==false)
            { 
                int c = 0;
                foreach (Button el in groupBox1.Controls)
                {
                    if (el.Enabled == false) c++;
                }
                if (c == 9)
                {
                    MessageBox.Show("Nobody won this round :( For the next one click the Next round button.");
                    rounds++;
                }
            }
        }

        private void button10_NextRound_Click(object sender, EventArgs e)
        {
            textBox1_round.Text = rounds.ToString();
            foreach (Button el in groupBox1.Controls)
            {
                if (el.Enabled == false) 
                    el.Enabled = true; 
            }
            foreach (Button el in groupBox1.Controls)
            {
                el.Text = string.Empty;
            }
                
            if(rounds % 2 == 0)
            {
                player1_previous = true;
                player2_previous = false;
            } else
            {
                player1_previous = false;
                player2_previous = true;
            }
            set_WhoNext();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
        
    }

