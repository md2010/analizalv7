﻿namespace XO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1_Player1 = new System.Windows.Forms.TextBox();
            this.textBox2_Player2 = new System.Windows.Forms.TextBox();
            this.button1_start = new System.Windows.Forms.Button();
            this.button2_quit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Sitka Text", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Sitka Text", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(9, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 40);
            this.label2.TabIndex = 1;
            this.label2.Text = "Player 2";
            // 
            // textBox1_Player1
            // 
            this.textBox1_Player1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox1_Player1.Location = new System.Drawing.Point(148, 97);
            this.textBox1_Player1.Name = "textBox1_Player1";
            this.textBox1_Player1.Size = new System.Drawing.Size(234, 34);
            this.textBox1_Player1.TabIndex = 2;
            // 
            // textBox2_Player2
            // 
            this.textBox2_Player2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox2_Player2.Location = new System.Drawing.Point(148, 202);
            this.textBox2_Player2.Name = "textBox2_Player2";
            this.textBox2_Player2.Size = new System.Drawing.Size(234, 34);
            this.textBox2_Player2.TabIndex = 3;
            // 
            // button1_start
            // 
            this.button1_start.BackColor = System.Drawing.Color.Beige;
            this.button1_start.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1_start.FlatAppearance.BorderSize = 2;
            this.button1_start.Font = new System.Drawing.Font("Sitka Text", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1_start.Location = new System.Drawing.Point(163, 299);
            this.button1_start.Name = "button1_start";
            this.button1_start.Size = new System.Drawing.Size(172, 82);
            this.button1_start.TabIndex = 4;
            this.button1_start.Text = "START";
            this.button1_start.UseVisualStyleBackColor = false;
            this.button1_start.Click += new System.EventHandler(this.button1_start_Click);
            // 
            // button2_quit
            // 
            this.button2_quit.BackColor = System.Drawing.Color.Gray;
            this.button2_quit.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2_quit.Location = new System.Drawing.Point(12, 392);
            this.button2_quit.Name = "button2_quit";
            this.button2_quit.Size = new System.Drawing.Size(97, 46);
            this.button2_quit.TabIndex = 5;
            this.button2_quit.Text = "QUIT";
            this.button2_quit.UseVisualStyleBackColor = false;
            this.button2_quit.Click += new System.EventHandler(this.button2_quit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label3.Font = new System.Drawing.Font("Showcard Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(400, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 50);
            this.label3.TabIndex = 6;
            this.label3.Text = "X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label4.Font = new System.Drawing.Font("Jokerman", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(398, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 55);
            this.label4.TabIndex = 7;
            this.label4.Text = "O";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(472, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2_quit);
            this.Controls.Add(this.button1_start);
            this.Controls.Add(this.textBox2_Player2);
            this.Controls.Add(this.textBox1_Player1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "XO";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1_Player1;
        private System.Windows.Forms.TextBox textBox2_Player2;
        private System.Windows.Forms.Button button1_start;
        private System.Windows.Forms.Button button2_quit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

