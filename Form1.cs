using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XO
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_start_Click(object sender, EventArgs e)
        {
            string player1Name = textBox1_Player1.Text;
            string player2Name = textBox2_Player2.Text;
            Player player1 = new Player(player1Name, 0,'x');
            Player player2 = new Player(player2Name, 0,'o');
            try
            {
                player1.checkNames();
                player2.checkNames();
                this.Hide();
                Form2 f2 = new Form2(textBox1_Player1.Text, textBox2_Player2.Text);
                f2.ShowDialog();
            }
            catch (EmptyNameException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (InvalidLengthException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }

    class EmptyNameException : Exception
    {
        public EmptyNameException() : base("Player's name is empty!") { }
        public EmptyNameException(String message) : base(message) { }
        public EmptyNameException(String message, Exception inner) : base(message, inner) { }
    }

    class InvalidLengthException : Exception
    {
        public InvalidLengthException() : base("Player's name is too long!") { }
        public InvalidLengthException(String message) : base(message) { }
        public InvalidLengthException(String message, Exception inner) : base(message, inner) { }
    }
    
}




