﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XO
{
    class Player
    {
        string name;
        int wins;
        char c;
        public Player(string name, int wins,char c)
        {
            this.name = name;
            this.wins = wins;
            this.c = c;
        }
        public void checkNames()
        {
            if (string.IsNullOrEmpty(this.name))
            {
                throw new EmptyNameException();

            }
            else if (this.name.Length > 15)
            {
                throw new InvalidLengthException();
            }
        }
        public string get_Name() 
        {
            return this.name;
        }

        public void set_Name(string new_name)
        {
            this.name = new_name;
        }
        public int get_Wins()
        {
            return this.wins;
        }
        public void set_Wins(int i)
        {
            this.wins += i;
        }
        public char get_Sign()
        {
            return this.c;
        }
    }
}
