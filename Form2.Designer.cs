﻿namespace XO
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button_2 = new System.Windows.Forms.Button();
            this.button_3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3_name1 = new System.Windows.Forms.Label();
            this.label4_name2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox1_round = new System.Windows.Forms.TextBox();
            this.textBox2_score1 = new System.Windows.Forms.TextBox();
            this.textBox3_score2 = new System.Windows.Forms.TextBox();
            this.button10_NextRound = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1_next = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 3;
            this.button1.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(13, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 121);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.FlatAppearance.BorderSize = 3;
            this.button4.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(13, 151);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(155, 121);
            this.button4.TabIndex = 1;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button5.FlatAppearance.BorderSize = 3;
            this.button5.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(174, 151);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(155, 121);
            this.button5.TabIndex = 2;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button6.FlatAppearance.BorderSize = 3;
            this.button6.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(335, 151);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(155, 121);
            this.button6.TabIndex = 3;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button7.FlatAppearance.BorderSize = 3;
            this.button7.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(13, 278);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(155, 121);
            this.button7.TabIndex = 4;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button8.FlatAppearance.BorderSize = 3;
            this.button8.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(174, 278);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(155, 121);
            this.button8.TabIndex = 5;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button9.FlatAppearance.BorderSize = 3;
            this.button9.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(335, 278);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(155, 121);
            this.button9.TabIndex = 6;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button_2
            // 
            this.button_2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_2.FlatAppearance.BorderSize = 3;
            this.button_2.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_2.Location = new System.Drawing.Point(174, 23);
            this.button_2.Name = "button_2";
            this.button_2.Size = new System.Drawing.Size(155, 121);
            this.button_2.TabIndex = 7;
            this.button_2.UseVisualStyleBackColor = false;
            this.button_2.Click += new System.EventHandler(this.button_2_Click);
            // 
            // button_3
            // 
            this.button_3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_3.FlatAppearance.BorderSize = 3;
            this.button_3.Font = new System.Drawing.Font("Curlz MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_3.Location = new System.Drawing.Point(335, 21);
            this.button_3.Name = "button_3";
            this.button_3.Size = new System.Drawing.Size(155, 121);
            this.button_3.TabIndex = 8;
            this.button_3.UseVisualStyleBackColor = false;
            this.button_3.Click += new System.EventHandler(this.button_3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button_2);
            this.groupBox1.Controls.Add(this.button_3);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Location = new System.Drawing.Point(12, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 414);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // label3_name1
            // 
            this.label3_name1.AutoSize = true;
            this.label3_name1.BackColor = System.Drawing.Color.Transparent;
            this.label3_name1.Font = new System.Drawing.Font("Sitka Text", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3_name1.Location = new System.Drawing.Point(630, 148);
            this.label3_name1.Name = "label3_name1";
            this.label3_name1.Size = new System.Drawing.Size(84, 35);
            this.label3_name1.TabIndex = 13;
            this.label3_name1.Text = "label3";
            // 
            // label4_name2
            // 
            this.label4_name2.AutoSize = true;
            this.label4_name2.BackColor = System.Drawing.Color.Transparent;
            this.label4_name2.Font = new System.Drawing.Font("Sitka Text", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4_name2.Location = new System.Drawing.Point(630, 309);
            this.label4_name2.Name = "label4_name2";
            this.label4_name2.Size = new System.Drawing.Size(85, 35);
            this.label4_name2.TabIndex = 14;
            this.label4_name2.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.DarkSalmon;
            this.label5.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(585, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 37);
            this.label5.TabIndex = 0;
            this.label5.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.DarkSalmon;
            this.label6.Font = new System.Drawing.Font("Jokerman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(584, 305);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 41);
            this.label6.TabIndex = 15;
            this.label6.Text = "O";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(541, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(225, 20);
            this.label7.TabIndex = 16;
            this.label7.Text = "________________________";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(589, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 29);
            this.label8.TabIndex = 17;
            this.label8.Text = "Score";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(588, 380);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 29);
            this.label9.TabIndex = 18;
            this.label9.Text = "Score";
            // 
            // textBox1_round
            // 
            this.textBox1_round.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox1_round.Location = new System.Drawing.Point(727, 12);
            this.textBox1_round.Name = "textBox1_round";
            this.textBox1_round.ReadOnly = true;
            this.textBox1_round.Size = new System.Drawing.Size(36, 34);
            this.textBox1_round.TabIndex = 19;
            this.textBox1_round.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2_score1
            // 
            this.textBox2_score1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox2_score1.Location = new System.Drawing.Point(659, 215);
            this.textBox2_score1.Name = "textBox2_score1";
            this.textBox2_score1.ReadOnly = true;
            this.textBox2_score1.Size = new System.Drawing.Size(36, 34);
            this.textBox2_score1.TabIndex = 20;
            this.textBox2_score1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3_score2
            // 
            this.textBox3_score2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox3_score2.Location = new System.Drawing.Point(658, 380);
            this.textBox3_score2.Name = "textBox3_score2";
            this.textBox3_score2.ReadOnly = true;
            this.textBox3_score2.Size = new System.Drawing.Size(36, 34);
            this.textBox3_score2.TabIndex = 21;
            this.textBox3_score2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button10_NextRound
            // 
            this.button10_NextRound.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button10_NextRound.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button10_NextRound.FlatAppearance.BorderSize = 3;
            this.button10_NextRound.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button10_NextRound.Location = new System.Drawing.Point(603, 498);
            this.button10_NextRound.Name = "button10_NextRound";
            this.button10_NextRound.Size = new System.Drawing.Size(160, 54);
            this.button10_NextRound.TabIndex = 22;
            this.button10_NextRound.Text = "NEXT ROUND";
            this.button10_NextRound.UseVisualStyleBackColor = false;
            this.button10_NextRound.Click += new System.EventHandler(this.button10_NextRound_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Gray;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button11.FlatAppearance.BorderSize = 2;
            this.button11.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button11.Location = new System.Drawing.Point(25, 498);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(120, 54);
            this.button11.TabIndex = 23;
            this.button11.Text = "QUIT";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Sitka Text", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(627, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 35);
            this.label1.TabIndex = 11;
            this.label1.Text = "Round: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Sitka Text", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(648, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 40);
            this.label2.TabIndex = 12;
            // 
            // textBox1_next
            // 
            this.textBox1_next.BackColor = System.Drawing.Color.DarkSlateGray;
            this.textBox1_next.Font = new System.Drawing.Font("Sitka Text", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox1_next.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox1_next.Location = new System.Drawing.Point(132, 31);
            this.textBox1_next.Name = "textBox1_next";
            this.textBox1_next.ReadOnly = true;
            this.textBox1_next.Size = new System.Drawing.Size(287, 36);
            this.textBox1_next.TabIndex = 24;
            this.textBox1_next.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form2
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(795, 564);
            this.Controls.Add(this.textBox1_next);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10_NextRound);
            this.Controls.Add(this.textBox3_score2);
            this.Controls.Add(this.textBox2_score1);
            this.Controls.Add(this.textBox1_round);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4_name2);
            this.Controls.Add(this.label3_name1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form2";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        //private System.Windows.Forms.TextBox textBox1_whichPlayerNext;
        //private System.Windows.Forms.TextBox textBox_Win1;
        //private System.Windows.Forms.Button button1_Next;
        //private System.Windows.Forms.Button button2;
        //private System.Windows.Forms.Button button3;
        //private System.Windows.Forms.TextBox textBox_Win2;
        
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button_2;
        private System.Windows.Forms.Button button_3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3_name1;
        private System.Windows.Forms.Label label4_name2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox1_round;
        private System.Windows.Forms.TextBox textBox2_score1;
        private System.Windows.Forms.TextBox textBox3_score2;
        private System.Windows.Forms.Button button10_NextRound;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1_next;
    }
}